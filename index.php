<?php

class Human {
	private $humanBody;
	private $humanBrain;

	public function __construct($body, $brain) {
		$this->humanBody = $body;
		$this->humanBrain = $brain;
	}
	public function getBodyAndBrain(){
		return $this->humanBody . ' ' . $this->humanBrain;
	}

}

class Person {
		public static function create($body, $brain){
			return new Human($body, $brain);
		}
	
}

$man = Person::create('This is', 'human');
print_r($man->getBodyAndBrain());

